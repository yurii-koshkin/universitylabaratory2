package org.labaratory.utils;

import org.labaratory.core.Customer;

import java.util.ArrayList;
import java.util.List;

public class CustomerUtils {
    public static List<Customer> getCustomersByName(List<Customer> customers, String name) {
        List<Customer> result = new ArrayList<>();

        for (Customer customer : customers) {
            if (customer.getName().equals(name)) {
                result.add(customer);
            }
        }

        return result;
    }

    public static List<Customer> getCustomersByCardNumber(List<Customer> customers, double minimumNumber, double maximumNumber) {
        List<Customer> result = new ArrayList<>();

        for (Customer customer : customers) {
            double cardNumber = Double.parseDouble(customer.getCreditCardNumber());
            if (cardNumber > minimumNumber && cardNumber < maximumNumber) {
                result.add(customer);
            }
        }

        return result;
    }

    public static List<Customer> getCustomersWithNegativeBalance(List<Customer> customers) {
        List<Customer> result = new ArrayList<>();

        for (Customer customer : customers) {
            if (customer.getBalance() < 0) {
                result.add(customer);
            }
        }

        return result;
    }
}
